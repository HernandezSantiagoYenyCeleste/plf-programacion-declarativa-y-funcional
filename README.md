# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
*[#Pink] Programación Declarativa. \norientaciones y pautas para el estudio
	*[#HotPink] Programación declarativa 
		*_ Es un
			*[#Pink] Estilo de programación (paradigma) que surge por \nproblemas 
				*_ Se lleva en:
					*[#Turquoise] La programación clásica
					*[#Turquoise] La programación imperativa
					*[#Turquoise] La programación en serie
					*[#Turquoise] etc
		*_ Proporciona
			*[#Pink] Detalles sobre los cálculos a realizar
		*_ A traves de
			*[#LightPink] La asignación mediante la cual se modifica \nel estado de la memoria del ordenador
				*_ Representa
					*[#Turquoise] Cuello de botella intelectual
		*_ La idea es:
			*[#LightPink] liberarse de las asignaciones
			*[#LightPink] liberarse de las especificaciones del control de memoria
			*[#LightPink] utilizar otros recursos
		*_ Consiste en
			*[#Violet] Las tareas rutinas de programación\n se dejan al compilador
			*[#Violet] Hacer un énfasis en los\n aspectos creativos de la programación
				*_ A partir de 
					*[#Turquoise] Algoritmos
					*[#Turquoise] Y que sea el ordenador (compilador)\n el que resuelva la gestión de memoria
			*[#Violet] Abandonar las secuencias de \nórdenes que gestionar la memoria
				*[#Turquoise] Explica los algoritmos
		*_ Ventajas
			*[#Violet] Programas más cortos y más fáciles de realizar y depurar
			*[#Violet] El tiempo de desarrollo de una aplicación es una décima parte
			*[#Violet] El tiempo de modificación y depuración es mucho menor
			*[#Violet] Facilita la programación
		*_ Propósito 
			*[#Violet] Reducir la complejidad de los programas
			*[#Violet] Reducir el riesgo de cometer errores 
			*[#Violet] Farragosidad del código que se escribió
		*_ Evolución de
			*[#LightPink] Los lenguajes de programación
				*_ A traves de:
					*[#Violet] Los computadores se le hablaba en código máquina
					*[#Violet] Los programas consisten en secuencias de órdenes directas
					*[#Violet] Se desarrollaron lenguajes de alto nivel
						*_ Ejemplo:
							*[#Turquoise] Fortran
		*_ Inconvenientes
			*[#LightPink] Ideas abstractas
			*[#LightPink] Eficiencia de los programas
			*[#LightPink] Ordenación 
				*_ Son
					*[#Violet] Problema crucial para resolver de forma muy eficiente
				*_ Es
					*[#Violet] Crucial cada asignación en memoria
			*[#LightPink] Al describir programas a nivel alto es el compilador \nel que toma las decisiones de manera automática
				*_ Entonces
					*[#Turquoise] Es muy difícil que sean eficientes
		*_ Variantes Principales
			*[#Violet] Programación funcional 
				*_ A traves de:
					*[#LightPink] lenguaje que utiliza los \nmatemáticos (describir funciones)
				*_ Representa:
					*[#LightPink] Una función que puede asimilar a un \nprograma
						*_ Tiene datos de 
							*[#Turquoise] Entrada (argumentos) 
							*[#Turquoise] Datos de salida (resultados de la función)
				*_ Ventajas 
					*[#LightPink] Funciones de orden superior
						*_ A traves de:
							*[#Turquoise] tener objetos principales
								*_ Que son:
									*[#White] Funciones que pueden definir funciones\n que actúan sobre funciones 
					*[#LightPink] Capacidad muy potente de los lenguajes funcionales
				*_ Características:
					*[#LightPink] Evaluación perezosa
						*_ Trata de:
							*[#Turquoise] Evalúar las ecuaciones
							*[#Turquoise] Calcular o computar lo que es necesario para ser cálculos posteriores
					*[#LightPink] Se puede definir tipos de datos infinitos
						*_ Ejemplo:
							*[#Turquoise] Serie de número de figonzi
					*[#LightPink] Existencia de funciones de orden superior 
						*_ Se aplica a
							*[#Turquoise] Programas a otros programas
							*[#Turquoise] Y programas a otros programas que modifican programas
				*_ Se basa en:
					*[#LightPink] Modelo de los matemáticos donde se definen funciones
				*_ Es:
					*[#LightPink] La aplicación de funciones a unos datos
						*_ Donde
							*[#Turquoise] La aplicación de funciones a esos datos \nse hacen mediante reglas de reescritura
								*_ Para:
									*[#Violet] Alcanzar el nivel de descripción superior

			*[#Violet] Programación lógica
				*_ A traves de:
					*[#LightPink] Lógica de predicados
						*[#Violet] No tenemos algo que corresponda \na un programa 
							*_ Pero 
								*[#Turquoise] Se tiene predicados 
									*_ Que son:
										*[#LightPink] Relaciones entre objetos que estamos definiendo 
										*[#LightPink] Estas relaciones no establecen orden entre
											*[#White] Argumentos de entrada
											*[#White] Datos de salida 
						*_ Ventaja
							*[#Violet] Nos permite ser más declarativos por lo \nque no establecemos la dirección de procesamiento \nentre argumentos y resultados 
								*_ Ejemplo:
									*[#Turquoise] Podemos definir una relación factorial \nentre dos números que nos dice si un \nnúmero es el factorial del otro
						*_ Forma de realizar cómputos
							*[#Violet] El intérprete, dado un conjunto de relaciones \ny predicador operan mediante algoritmos de resolución \ne intenta demostrar predicados dentro del sistema
								*_ Ejemplos:
									*[#Turquoise] Si el factorial de 2 es i, si i es una variable \nlo que pasa con el intérprete es intentar encontrar \ncuales son los valores para la variable i que cumplan
									*[#Turquoise] Si el factorial de x es 6, como variable x se\n intenta encontrar todos los valores de x que satisfacen el factorial
				*_ Se basa en:
					*[#LightPink] Modelo de la demostración de la lógica y automática
						*_ Se utilizan como 
							*[#Violet] Expresiones de los programas
							*[#Violet] Axiomas
							*[#Violet] Reglas de inferencia
					*[#LightPink] El compilador es un motor de inferencia
						*_ Que a partir de expresiones
							*[#Violet] Razona
							*[#Violet] Da respuesta
			*[#Violet] Diferencias 
				*[#LightPink] Programación funcional
					*_ Utiliza
						*[#Violet] Metáfora por la que los programas \n(funciones) aplican funciones a datos completos.
					*_ Mediante
						*[#Violet] Reglas de simplificación
					*_ Funciones de
						*[#Violet] Orden superior
							*_ Como
								*[#Turquoise] La entrada de un programa \npuede ser otro programa
					*[#Violet] Evaluación perezosa
				*[#LightPink] Programación Lógica
					*_ Se estudia
						*[#Turquoise] Dentro del paradigma declarativa
					*_ Utiliza
						*[#Turquoise] La demostración automática para programar
							*_ Se hace sobre las reglas
								*[#Violet] Inferencia
								*[#Violet] Razonamiento 
					*_ No hay distinción entre 
						*[#Turquoise] Datos de entrada
						*[#Turquoise] Datos de salida
					*[#Turquoise] Múltiples aplicaciones
	*[#HotPink] Programación imperativa
		*_ Se basa en
			*[#LightPink] Modelo de Jhon Von Neuman
		*_ Busca
			*[#LightPink] Alternativas expresivas\n para separar de este modelo
				*_ Desaarrolla
					*[#Violet] Especificaciones\n sobre qué es lo que queremos computar
		*_ Según el tipo de 
			*[#LightPink] Lenguajes
			*[#LightPink] Recursos 
@endmindmap
```

# Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
*[#GreenYellow] Paradigmas de programación
	*_ Que son
		*[#PaleGreen] Son el modelo de computación el lo que los \ndiferentes lenguajes dotan de semántica a los programas
	*_ Basados en el modelo
		*[#Turquoise] Al principio todos los lenguajes estaban\n basados en el modelo de John von Neumann
			*[#PaleGreen] Los programas deben almacenarse en la \nmisma máquina antes de ser ejecutados
			*[#PaleGreen] Lenguaje imperativo
	*_ Variante
		*[#PaleGreen] Programación Orientada a Objetos
		*[#PaleGreen] Lógica simbólica
			*_ Que son
				*[#Turquoise] Paradigma de programación lógico
					*_ Representa
						*[#White] Conjunto de sentencias que definen lo que \nes verdad y lo que es conocido para un programa
	*[#LawnGreen] Landa Cálculo
		*_ En el año
			*[#PaleGreen] 1930
		*_ Sistema
			*[#PaleGreen] Para estudiar función, recursividad
			*[#PaleGreen] Potente equivalente \nal modelo de John Neumann
				*_ Desarrollo
					*[#Turquoise] Lógica combinatoria
				*_ En
					*[#Turquoise] 1965 peter landin
						*_ Desarrollo
							*[#White] Cimientos para un lenguaje\n de programacion
								*_ Que son
									*[#PaleGreen] Bases para la programación\n funcional
		*[#PaleGreen] Currificación
			*_ Es
				*[#Turquoise] Una función con n parámetros y devuelve un parámetro \nde salida puede ser vista como una función que tiene un único \nparametro de entrada y que devuelve una función con n-1 parámetros de entrada
				*[#Turquoise] Aplicación parcial 
			*_ En honor a 
				*[#Turquoise] Haskell Brooks Curry
	*[#LawnGreen] Rasgos de las bases
		*_ Que es un programa?
			*[#Turquoise] Es una colección de datos, serie de \ninstrucciones que operan sobre dichos datos
		*_ Se ejecuntan
			*[#Turquoise] Las instrucciones en un orden adecuado
				*[#PaleGreen] Se puede modifican según el \nvalor de las variables
	*[#LawnGreen] Programación funcional
		*_ Son
			*[#Turquoise] Solamente funciones matemáticas
				*_ Que reciben
					*[#PaleGreen] Parámetros de entrada
				*_ Devuelven
					*[#PaleGreen] Salidas
		*_ Consecuencias
			*[#Turquoise] La asignación
		*_ Que son los Bucles
			*[#Turquoise] Serie de instrucciones que se repiten un número \nindeterminado de veces y están controlados\n por el estado del computador
		*_ Desaparece
			*[#Turquoise] El concepto de variable
				*_ Pero
					*[#PaleGreen] Existe pero solamente para referirse a \nlos parámetros de entrada de las funciones
						*_ Entonces
							*[#White] Solo almacena temporalmente el \nvalor de parámetro de entrada 
		*_ Constante 
			*[#Turquoise] Las constantes equiparan a las funciones
		*_ Ventajas
			*[#Turquoise] Transparencia referencia
				*_ Dependen de 
					*[#PaleGreen] Los programas solo depender de\n los parámetros de entrada
				*_ Una funcion
					*[#PaleGreen] Recibe los parámetros de entrada
					*[#PaleGreen] Devuelve los mismos parámetros
				*_ Los resultados son
					*[#PaleGreen] independientes del orden en que se realizan los cálculos
						*[#White] El orden no es relevante en absoluto
		*_ Son 
			*[#Turquoise] Todas funciones
				*_ Que pueden ser
					*[#PaleGreen] Parámetros de otras funciones
						*_ Que se denominan
							* De orden superior
		*_ Evaluacion
			*[#Turquoise] Impaciente o Estricta
				*_ Evalúa
					*[#PaleGreen] Las expresiones desde andentro hacia afuera
						* Primero lo más interno y hasta que se\n termine de evaluar no evalúa lo externo
				*_ Es
					*[#PaleGreen] Sencilla de implementar
				*_ Inconveniente
					*[#PaleGreen] Si no se puede evaluar una expresión en \ntiempo finito
						* el cálculo se interrumpa o produce error
		*_ Evaluación no estricta 
			*_ Evalua
				*[#Turquoise] Desde afuera hacia dentro
			*_ Es
				*[#Turquoise] Complicada de implementar 
			*[#PaleGreen] Función que no necesita conocer el valor\n de uno de sus parámetros para devolver el resultado
	*[#LawnGreen] Programación Imperativa
		*_ En una funcion se puede
			*[#PaleGreen] Haber efectos colaterales
			*[#PaleGreen] modificar el estado del cómputo paralelamente
		*_ Evaluacion
			*[#PaleGreen] En cortocircuito o McCarthy
				*_ Es una evaluacion
					*[#Turquoise] Muy costosa
					*[#Turquoise] Estricta seria una única vez
	*[#LawnGreen] Entorno multiprocesador
		*_ Ventaja
			*[#Turquoise] El resultado de las funciones no dependen\n de las otras, se pueden paralizar sin problemas
	*[#LawnGreen] Utilidad real
		*_ Utilidad
			*[#Turquoise] Permiten realizar prototipos muy rápidamente
			*[#Turquoise] Eficiencia con respecto a la programación
				*_ Ejemplo
					* Qué tan rápido puede escribir el programa
			*[#Turquoise] Costo computacional mayor	
	*[#LawnGreen] En la vida real 
		*_ Se ocupa para
			*[#PaleGreen] Sudoku
			*[#PaleGreen] Juegos de arcade
			*[#PaleGreen] Se puede hacer cualquier cosa
				*[#Turquoise] Programar un word
@endmindmap
```